# Yl954 Individual Project 4
This project implemented Rust AWS Lambda and Step Functions.

## Demo Video
![](demoVideo.webm)

## Rust Lambda Functionality
```s1``` can read an input string, and then count the number of each numbers. This lambda function will return a map. For example, when you input ```{"stringData": "12312413412312412323132365463"}```
, it will return ```{"mapData":{"1":7,"2":7,"3":8,"4":4,"5":1,"6":2}}```.

```s2``` can read an input map that contains numbers and counts, and then return the number with the maximum count value. For example, when you input ```{"mapData":{"1":7,"2":7,"3":8,"4":4,"5":1,"6":2}}```, it will return ```"3"```.

## Step Functions Workflow
![](image.png)

### Execution
![](image-1.png)
![](image-2.png)
![](image-3.png)
Here we inputed ```{"stringData": "1232"}```, and got the output ```"2"```. This meets our expectation.

## Data Processing Pipeline
1. ```s1``` reads an input string and count the number of each numbers in the string. The output is a BTreeMap containing the numbers (key) and their counts (value).

2. ```s2``` gets the output from ```s1``` as input. Then it will compare the counts of each number, and output the one with the largest count.

## Auto Deploy By CI/CD
```
stages:
  - deploy

deploy:
  stage: deploy
  image: calavera/cargo-lambda:latest
  script:
    - export AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID
    - export AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY
    - export AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION
    - cd s1
    - cargo lambda build --release
    - cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::471112888499:role/for_cargo_lambda
    - cd ../s2
    - cargo lambda build --release
    - cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::471112888499:role/for_cargo_lambda
```
