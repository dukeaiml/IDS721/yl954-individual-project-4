use serde::{Deserialize, Serialize};
use lambda_runtime::{handler_fn, Context, Error};
use std::collections::{HashMap, BTreeMap};

#[derive(Deserialize, Serialize)]
struct MyMap {
    mapData: BTreeMap<char, usize>
}

async fn function_handler(event: MyMap, _ctx: Context) -> Result<char, Error> {
    let mut max_cnt = 0;
    let mut max_num = '0';
    for (c, cnt) in event.mapData {
        if max_cnt < cnt {
            max_cnt = cnt;
            max_num = c;
        }
    }
    Ok(max_num)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(function_handler)).await?;
    Ok(())
}
