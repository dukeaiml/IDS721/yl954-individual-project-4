use serde::{Deserialize, Serialize};
use lambda_runtime::{handler_fn, Context, Error};
use std::collections::{HashMap, BTreeMap};

#[derive(Deserialize, Serialize)]
struct MyMap {
    mapData: BTreeMap<char, usize>
}

#[derive(Deserialize, Serialize)]
struct MyString {
    stringData: String
}

async fn function_handler(event: MyString, _ctx: Context) -> Result<MyMap, Error> {
    let mut res = HashMap::new();
    for c in event.stringData.chars() {
        if c.is_numeric() {
            let counter = res.entry(c.to_ascii_lowercase()).or_insert(0);
            *counter += 1;
        }
    }
    Ok(MyMap { mapData: res.into_iter().collect() })
}


#[tokio::main]
async fn main() -> Result<(), Error> {
    lambda_runtime::run(handler_fn(function_handler)).await?;
    Ok(())
}
